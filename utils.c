#include "utils.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int isNumber(char str[])
{
	int i;

	for (i = 0; i < strlen(str); i++)
		if (str[i] < '0' || str[i] > '9')
			return -1;
	return 1;
}

tabel *creare_tabel(unsigned int marime)
{
	int i;
	tabel *hashmap;

	/*aloc tabelul*/
	hashmap = malloc(sizeof(tabel));
	if (hashmap == NULL)
		return NULL; /*alocarea nu a reusit*/

	hashmap->buckets = (lista **) calloc(marime, sizeof(lista *));
	if (hashmap->buckets == NULL)
		return NULL;/*alocarea nu a reusit*/

	for (i = 0; i < marime; i++)
		hashmap->buckets[i] = NULL;/*aloc fiecare bucket*/

	hashmap->marime = marime;

	return hashmap;

}

int adauga_element(tabel *hashmap, char *element)
{
	lista *celula, *nod, *aux;
	unsigned int pozitie = hash(element, hashmap->marime);

	nod = hashmap->buckets[pozitie];
	celula = malloc(sizeof(lista *));
	if (celula == NULL)
		return -1;/*alocarea nu a reusit*/
	celula->urm = NULL;
	celula->info = malloc(strlen(element) + 1);
	strcpy(celula->info, element);


	/*inserarea in hash*/
	if (nod == NULL) {/*inserare la inceputul listei*/
		hashmap->buckets[pozitie] = celula;
		return 1;
	}

	for (; nod != NULL; nod = nod->urm) {
		if (strcmp(nod->info, element) == 0)
			return -1; /*elementul exista deja*/
		aux = nod;

	} /*merg pana la ultimul element din lista*/



	aux->urm = celula;




	return 1;


}


int sterge_element(tabel *hashmap, char *element)
{
	unsigned int pozitie = hash(element, hashmap->marime);
	lista *nod, *aux;
	int i;
	int marime = hashmap->marime;


	nod = hashmap->buckets[pozitie];
	if (nod != NULL) {
		if (strcmp(element, nod->info) == 0) {
			hashmap->buckets[pozitie] = nod->urm;
			free(nod);
			return 0;
		}
		for (i = 0; i < marime; i++) {
			nod = hashmap->buckets[i];


			for (; nod != NULL; nod = nod->urm) {
				if (strcmp(element, nod->info) == 0) {
					aux->urm = nod->urm;
					free(nod);
					return 0;
				}
				aux = nod;


			}
		}
	}

	return 1;


}

int afis_bucket(tabel *hashmap, int nrBucket, FILE *output)
{
	unsigned int marime = hashmap->marime;
	lista *nod;

	/*verific daca bucketul e valid*/
	if (nrBucket >= 0 && nrBucket < marime &&
		hashmap->buckets[nrBucket] != NULL) {
		nod = hashmap->buckets[nrBucket];

		for (; nod != NULL; nod = nod->urm) {
			if (nod->urm != NULL)
				fprintf(output, "%s ", nod->info);
			else
				fprintf(output, "%s", nod->info);

		}
		fprintf(output, "\n");
	}

	return 1;
}

int afis_hash(tabel *hashmap, FILE *output)
{
	unsigned int marime = hashmap->marime;
	unsigned int i;

	/*afisez pe rand fiecar bucket */
	for (i = 0; i < marime; i++)
		afis_bucket(hashmap, i, output);

	return 1;
}

int gaseste(tabel *hashmap, char *cuvant, FILE *output)
{
	unsigned int marime = hashmap->marime;
	lista *nod;
	unsigned int i;

	/*verific pe rand fiecare element*/
	for (i = 0; i < marime; i++) {
		nod = hashmap->buckets[i];
		for (; nod != NULL; nod = nod->urm) {
			if (strcmp(cuvant, nod->info) == 0) {
				fprintf(output, "True\n");
				return 0;
			}
		}
	}
	/*daca nu gasesc elementul afisez fals*/
	fprintf(output, "False\n");
	return -1;

}

int clear(tabel *hashmap)
{
	unsigned int marime = hashmap->marime;
	unsigned int i;
	lista *nod, *aux;

	for (i = 0; i < marime; i++) {
		nod = hashmap->buckets[i];
		while (nod != NULL) {
			aux = nod;
			nod = nod->urm;
			free(aux->info);
			aux->urm = NULL;
			free(aux);

		}
		hashmap->buckets[i] = NULL;

	}


	return 1;

}

int redimensionare(tabel *hashmap, unsigned int marime_noua)
{
	unsigned int i;
	tabel *hash_nou;
	lista *nod;

	hash_nou = creare_tabel(marime_noua);
	if (hash_nou == NULL)
		return -1;

	/*copiez toate elementele*/
	for (i = 0; i < hashmap->marime; i++) {
		nod = hashmap->buckets[i];
		if (nod != NULL) {
			while (nod != NULL) {
				adauga_element(hash_nou, nod->info);
				nod = nod->urm;
			}

		}
	}

	clear(hashmap);
	free(hashmap->buckets);


	hashmap->marime = hash_nou->marime;

	hashmap->buckets = hash_nou->buckets;

	return 0;


}


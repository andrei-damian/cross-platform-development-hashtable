
#include <stdio.h>
#include "hash.h"

typedef struct lista {
	char *info;
	struct lista *urm;
} lista;

typedef struct hashtable {
	unsigned int marime;

	lista **buckets;
} tabel;

#define lungime_buffer 20000

tabel *creare_tabel(unsigned int marime);
int adauga_element(tabel *hashmap, char *element);
int sterge_element(tabel *hashmap, char *element);
int afis_bucket(tabel *hashmap, int nrBucket, FILE *output);
int afis_hash(tabel *hashmap, FILE *output);
int gaseste(tabel *hashmap, char *cuvant, FILE *output);
int clear(tabel *hashmap);
int redimensionare(tabel *hashmap, unsigned int marime_noua);
int isNumber(char str[]);

#include <errno.h>
#define DIE(assertion, call_description)\
	do {\
		if (assertion) {\
			fprintf(stderr, "(%s, %d): ",\
					__FILE__, __LINE__);\
			perror(call_description);\
			exit(errno);\
		} \
	} while (0)

#include <stdio.h>
#include <string.h>
#include "utils.h"
#include <stdlib.h>


int parsare_comanda(tabel *hashmap, char *buffer)
{
	char *tok;
	char **cuvinte = calloc(3, sizeof(char *));
	int i = 0;
	FILE *output = stdout;

	tok = strtok(buffer, "\n ");


	while (tok != NULL) {

		cuvinte[i] = tok;
		i++;
		tok = strtok(NULL, "\n ");
	}

	if (cuvinte[0] == NULL)
		return -2;
	if (strcmp(cuvinte[0], "add") == 0 && cuvinte[1] != NULL) {
		adauga_element(hashmap, cuvinte[1]);
		return 1;
	} else if (strcmp(cuvinte[0], "remove") == 0) {
		sterge_element(hashmap, cuvinte[1]);
		return 1;
	} else if (strcmp(cuvinte[0], "find") == 0) {
		if (i < 3) {
			gaseste(hashmap, cuvinte[1], output);
			return 1;
		}
		output = fopen(cuvinte[2], "a+");
		gaseste(hashmap, cuvinte[1], output);
		fclose(output);
		output = stdout;
		return 1;
	} else if (strcmp(cuvinte[0], "clear") == 0) {

		clear(hashmap);
		return 1;
	}

	if (strcmp(cuvinte[0], "print") == 0) {
		if (i < 2) {
			afis_hash(hashmap, output);
			return 1;
		}
		output = fopen(cuvinte[1], "a+");
		afis_hash(hashmap, output);
		fclose(output);
		output = stdout;
		return 1;
	} else if (strcmp(cuvinte[0], "print_bucket") == 0 &&
		isNumber(cuvinte[1]) > 0) {
		if (i < 3) {
			afis_bucket(hashmap, atoi(cuvinte[1]), output);
			return 1;
		}
		output = fopen(cuvinte[2], "a+");
		afis_bucket(hashmap, atoi(cuvinte[1]), output);
		fclose(output);
		output = stdout;
		return 1;
	} else if (strcmp(cuvinte[0], "resize") == 0 &&
		strcmp(cuvinte[1], "double") == 0) {
		redimensionare(hashmap, hashmap->marime * 2);
		return 1;
	} else if (strcmp(cuvinte[0], "resize") == 0 &&
		strcmp(cuvinte[1], "halve") == 0) {
		redimensionare(hashmap, hashmap->marime / 2);
		return 1;
	} else
		return -1;


}

int main(int argc, char *argv[])
{
	char line[lungime_buffer];
	tabel *hashmap;
	int i;
	FILE *fp;
	int returnat = -1;

	DIE(argc < 2, "parametrii invalizi");
	DIE(isNumber(argv[1]) < 0, "parametrii gresiti");
	hashmap = creare_tabel(atoi(argv[1]));


	if (argc == 2) {
		while (fgets(line, lungime_buffer, stdin)) {
			returnat = parsare_comanda(hashmap, line);
			DIE(returnat == -1, "comanda invalida");

		}

	}


	if (argc >= 3) {

		for (i = 2; i < argc; i++) {
			fp = fopen(argv[i], "r");
			if (fp != NULL)
				while (fgets(line, lungime_buffer, fp)) {
					returnat =
					parsare_comanda(hashmap, line);
					DIE(returnat == -1, "comanda invalida");

				}

		}


	}
	clear(hashmap);

}
